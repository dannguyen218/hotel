package com.codegym.service.bookings;

import com.codegym.model.Bookings;

import java.util.List;

public interface BookingsService {
    List<Bookings> findAll();

    void save(Bookings bookings);

    Bookings findById(int id);

    void update(Bookings bookings);

    void remove(int id, Bookings bookings);
}
