package com.codegym.service.bookings;

import com.codegym.model.Bookings;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookingsJDBCServiceImpl implements BookingsService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/case_study_version1";
    private String jdbcUsername = "root";
    private String jdbcPassword = "12345678";

    private static final String INSERT_USER_SQL = "INSERT INTO Bookings" + "(Day_reservation, Day_check_in, Day_check_out, Payments, Amount_all, Money_type, User_check_in, User_check_out, Users_ID, Booking_Deposits_ID, Customers_ID, created_at, created_by) VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?);";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM Bookings WHERE ID = ?;";
    private static final String SELECT_ALL_USERS = "SELECT * FROM Bookings WHERE isDeleted = 0;";
    private static final String DELETE_USER_SQL = "UPDATE Bookings SET isDeleted = 1, deleted_at = now(), deleted_by = ? WHERE ID = ?;";
    private static final String UPDATE_USER_SQL = "UPDATE Bookings SET Day_reservation = ?, Day_check_in = ?, Day_check_out = ?, Payments = ?, Amount_all = ?, Money_type = ?, User_check_in = ?, User_check_out = ?, Users_ID = ?, Booking_Deposits_ID = ?, Customers_ID = ?, updated_at = now(), updated_by = ? WHERE ID = ?;";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Bookings> findAll() {
        List<Bookings> bookings = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("ID");
                String day_reservation = rs.getString("Day_reservation");
                String day_check_in = rs.getString("Day_check_in");
                String day_check_out = rs.getString("Day_check_out");
                String payments = rs.getString("Payments");
                String amount_all = rs.getString("Amount_all");
                String money_type = rs.getString("Money_type");
                int user_check_in = rs.getInt("User_check_in");
                int user_check_out = rs.getInt("User_check_out");
                int users_id = rs.getInt("Users_ID");
                int booking_deposits_id = rs.getInt("Booking_Deposits_ID");
                int customers_id = rs.getInt("Customers_ID");
                bookings.add(new Bookings(id, day_reservation, day_check_in, day_check_out, payments, amount_all, money_type, user_check_in, user_check_out, users_id, booking_deposits_id, customers_id));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return bookings;
    }

    @Override
    public void save(Bookings bookings) {
        System.out.println(INSERT_USER_SQL);

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {

            preparedStatement.setString(1, bookings.getDay_reservation());
            preparedStatement.setString(2, bookings.getDay_check_in());
            preparedStatement.setString(3, bookings.getDay_check_out());
            preparedStatement.setString(4, bookings.getPayments());
            preparedStatement.setString(5, bookings.getAmount_all());
            preparedStatement.setString(6, bookings.getMoney_type());
            preparedStatement.setInt(7, bookings.getUser_check_in());
            preparedStatement.setInt(8, bookings.getUser_check_out());
            preparedStatement.setInt(9, bookings.getUsers_ID());
            preparedStatement.setInt(10, bookings.getBooking_Deposits_ID());
            preparedStatement.setInt(11, bookings.getCustomers_ID());
            preparedStatement.setString(12, bookings.getCreated_by());

            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Bookings findById(int id) {
        Bookings bookings = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {

            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String day_reservation = rs.getString("Day_reservation");
                String day_check_in = rs.getString("Day_check_in");
                String day_check_out = rs.getString("Day_check_out");
                String payments = rs.getString("Payments");
                String amount_all = rs.getString("Amount_all");
                String money_type = rs.getString("Money_type");
                int user_check_in = rs.getInt("User_check_in");
                int user_check_out = rs.getInt("User_check_out");
                int users_id = rs.getInt("Users_ID");
                int booking_deposits_id = rs.getInt("Booking_Deposits_ID");
                int customers_id = rs.getInt("Customers_ID");
                bookings = new Bookings(id, day_reservation, day_check_in, day_check_out, payments, amount_all, money_type, user_check_in, user_check_out, users_id, booking_deposits_id, customers_id);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return bookings;
    }

    @Override
    public void update(Bookings bookings) {
        boolean rowUpdated;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL)) {
            statement.setString(1, bookings.getDay_reservation());
            statement.setString(2, bookings.getDay_check_in());
            statement.setString(3, bookings.getDay_check_out());
            statement.setString(4, bookings.getPayments());
            statement.setString(5, bookings.getAmount_all());
            statement.setString(6, bookings.getMoney_type());
            statement.setInt(7, bookings.getUser_check_in());
            statement.setInt(8, bookings.getUser_check_out());
            statement.setInt(9, bookings.getUsers_ID());
            statement.setInt(10, bookings.getBooking_Deposits_ID());
            statement.setInt(11, bookings.getCustomers_ID());
            statement.setString(12, bookings.getUpdated_by());
            statement.setInt(13, bookings.getId());

            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public void remove(int id, Bookings bookings) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USER_SQL)) {

            statement.setString(1, bookings.getDeleted_by());
            statement.setInt(2, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }
}
