package com.codegym.service.booking_details;

import com.codegym.model.Booking_Details;

import java.util.List;

public interface Booking_DetailsService {
    List<Booking_Details> findAll();

    void save(Booking_Details booking_details);

    Booking_Details findById(int id);

    void update(Booking_Details booking_details);

    void remove(int id, Booking_Details booking_details);
}
