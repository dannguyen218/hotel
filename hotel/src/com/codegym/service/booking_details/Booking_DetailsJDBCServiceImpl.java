package com.codegym.service.booking_details;

import com.codegym.model.Booking_Details;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Booking_DetailsJDBCServiceImpl implements Booking_DetailsService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/case_study_version1";
    private String jdbcUsername = "root";
    private String jdbcPassword = "12345678";

    private static final String INSERT_USER_SQL = "INSERT INTO Booking_Details" + "(Room_name, Type_room, Price, Money_deposit, Money_type, Day_booking, User_id_in, Day_out, User_id_out, Booking_Room_ID, created_at, created_by) VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?);";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM Booking_Details WHERE ID = ?;";
    private static final String SELECT_ALL_USERS = "SELECT * FROM Booking_Details WHERE isDeleted = 0;";
    private static final String DELETE_USER_SQL = "UPDATE Booking_Details SET isDeleted = 1, deleted_at = now(), deleted_by = ? WHERE ID = ?;";
    private static final String UPDATE_USER_SQL = "UPDATE Booking_Details SET Room_name = ?, Type_room = ?, Price = ?, Money_deposit = ?, Money_type = ?, Day_booking = ?, User_id_in = ?, Day_out = ?, User_id_out = ?, Booking_Room_ID = ?, updated_at = now(), updated_by = ? WHERE ID = ?;";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Booking_Details> findAll() {
        List<Booking_Details> booking_details = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("ID");
                String room_name = rs.getString("Room_name");
                String type_room = rs.getString("Type_room");
                String price = rs.getString("Price");
                String money_deposit = rs.getString("Money_deposit");
                String money_type = rs.getString("Money_type");
                String day_booking = rs.getString("Day_booking");
                String user_id_in = rs.getString("User_id_in");
                String day_out = rs.getString("Day_out");
                String user_id_out = rs.getString("User_id_out");
                int booking_room_id = rs.getInt("Booking_Room_ID");
                booking_details.add(new Booking_Details(id, room_name, type_room, price, money_deposit, money_type, day_booking, user_id_in, day_out, user_id_out, booking_room_id));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return booking_details;
    }

    @Override
    public void save(Booking_Details booking_details) {
        System.out.println(INSERT_USER_SQL);

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {

            preparedStatement.setString(1, booking_details.getRoom_name());
            preparedStatement.setString(2, booking_details.getType_room());
            preparedStatement.setString(3, booking_details.getPrice());
            preparedStatement.setString(4, booking_details.getMoney_deposit());
            preparedStatement.setString(5, booking_details.getMoney_type());
            preparedStatement.setString(6, booking_details.getDay_booking());
            preparedStatement.setString(7, booking_details.getUser_id_in());
            preparedStatement.setString(8, booking_details.getDay_out());
            preparedStatement.setString(9, booking_details.getUser_id_out());
            preparedStatement.setInt(10, booking_details.getBooking_Room_ID());
            preparedStatement.setString(11, booking_details.getCreated_by());

            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Booking_Details findById(int id) {
        Booking_Details booking_details = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {

            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String room_name = rs.getString("Room_name");
                String type_room = rs.getString("Type_room");
                String price = rs.getString("price");
                String money_deposit = rs.getString("money_deposit");
                String money_type = rs.getString("Money_type");
                String day_booking = rs.getString("Day_booking");
                String user_id_in = rs.getString("User_id_in");
                String day_out = rs.getString("Day_out");
                String user_id_out = rs.getString("User_id_out");
                int booking_room_id = rs.getInt("Booking_Room_ID");

                booking_details = new Booking_Details(id, room_name, type_room, price, money_deposit, money_type, day_booking, user_id_in, day_out, user_id_out, booking_room_id);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return booking_details;
    }

    @Override
    public void update(Booking_Details booking_details) {
        boolean rowUpdated;

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL)) {
            statement.setString(1, booking_details.getRoom_name());
            statement.setString(2, booking_details.getType_room());
            statement.setString(3, booking_details.getPrice());
            statement.setString(4, booking_details.getMoney_deposit());
            statement.setString(5, booking_details.getMoney_type());
            statement.setString(6, booking_details.getDay_booking());
            statement.setString(7, booking_details.getUser_id_in());
            statement.setString(8, booking_details.getDay_out());
            statement.setString(9, booking_details.getUser_id_out());
            statement.setInt(10, booking_details.getBooking_Room_ID());
            statement.setString(11, booking_details.getUpdated_by());
            statement.setInt(12, booking_details.getId());

            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public void remove(int id, Booking_Details booking_details) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USER_SQL)) {

            statement.setString(1, booking_details.getDeleted_by());
            statement.setInt(2, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }
}
