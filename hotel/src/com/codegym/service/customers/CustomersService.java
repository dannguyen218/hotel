package com.codegym.service.customers;


import com.codegym.model.Customers;

import java.util.List;

public interface CustomersService {
    List<Customers> findAll();
}
