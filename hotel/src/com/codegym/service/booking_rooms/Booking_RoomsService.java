package com.codegym.service.booking_rooms;

import com.codegym.model.Booking_Rooms;

import java.util.List;

public interface Booking_RoomsService {
    List<Booking_Rooms> findAll();
}
