package com.codegym.service.users;



import com.codegym.model.Users;

import java.util.List;

public interface UsersService {
    List<Users> findAll();
}
