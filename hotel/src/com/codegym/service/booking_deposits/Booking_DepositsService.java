package com.codegym.service.booking_deposits;

import com.codegym.model.Booking_Deposits;

import java.util.List;

public interface Booking_DepositsService {
    List<Booking_Deposits> findAll();
}
