package com.codegym.controller;

import com.codegym.model.Booking_Details;
import com.codegym.model.Booking_Rooms;
import com.codegym.service.booking_details.Booking_DetailsJDBCServiceImpl;
import com.codegym.service.booking_details.Booking_DetailsService;
import com.codegym.service.booking_rooms.Booking_RoomsJDBCServiceImpl;
import com.codegym.service.booking_rooms.Booking_RoomsService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet(name = "Booking_DetailsServlet", urlPatterns = "/booking_details")
public class Booking_DetailsServlet extends HttpServlet {
    private Booking_DetailsService booking_detailsService = new Booking_DetailsJDBCServiceImpl();
    private Booking_RoomsService booking_roomsService = new Booking_RoomsJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                createBooking_Details(request, response);
                break;
            case "edit":
                updateBooking_Details(request, response);
                break;
            case "delete":
                deleteBooking_Details(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewBooking_Details(request, response);
                break;
            default:
                listBooking_Details(request, response);
        }
    }

    private void listBooking_Details(HttpServletRequest request, HttpServletResponse response) {
        List<Booking_Details> booking_details = this.booking_detailsService.findAll();
        request.setAttribute("booking_details", booking_details);

        RequestDispatcher dispatcher = request.getRequestDispatcher("booking_details/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void viewBooking_Details(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Booking_Details booking_details = this.booking_detailsService.findById(id);

        RequestDispatcher dispatcher;
        if (booking_details == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("booking_details", booking_details);
            dispatcher = request.getRequestDispatcher("booking_details/view.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createBooking_Details(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        String room_name = request.getParameter("room_name");
        String type_room = request.getParameter("type_room");
        String price = request.getParameter("price");
        String money_deposit = request.getParameter("money_deposit");
        String money_type = request.getParameter("money_type");
        String day_booking = request.getParameter("day_booking");
        String user_id_in = request.getParameter("user_id_in");
        String day_out = request.getParameter("day_out");
        String user_id_out = request.getParameter("user_id_out");
        int booking_room_id = Integer.parseInt(request.getParameter("listBooking_Rooms"));

        Booking_Details booking_details = new Booking_Details(room_name, type_room, price, money_deposit, money_type, day_booking, user_id_in, day_out, user_id_out, booking_room_id);
        this.booking_detailsService.save(booking_details);

        List<Booking_Rooms> booking_rooms = this.booking_roomsService.findAll();
        request.setAttribute("booking_rooms", booking_rooms);
        dispatcher = request.getRequestDispatcher("booking_details/create.jsp");
        request.setAttribute("message", "New booking detail was created");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        List<Booking_Rooms> booking_rooms = this.booking_roomsService.findAll();
        request.setAttribute("booking_rooms", booking_rooms);

        RequestDispatcher dispatcher = request.getRequestDispatcher("booking_details/create.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateBooking_Details(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String room_name = request.getParameter("room_name");
        String type_room = request.getParameter("type_room");
        String price = request.getParameter("price");
        String money_deposit = request.getParameter("money_deposit");
        String money_type = request.getParameter("money_type");
        String day_booking = request.getParameter("day_booking");
        String user_id_in = request.getParameter("user_id_in");
        String day_out = request.getParameter("day_out");
        String user_id_out = request.getParameter("user_id_out");
        int booking_room_id = Integer.parseInt(request.getParameter("listBooking_Rooms"));
        Booking_Details booking_details = this.booking_detailsService.findById(id);

        List<Booking_Rooms> booking_rooms = this.booking_roomsService.findAll();

        RequestDispatcher dispatcher;
        if (booking_details == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            booking_details.setRoom_name(room_name);
            booking_details.setType_room(type_room);
            booking_details.setPrice(price);
            booking_details.setMoney_deposit(money_deposit);
            booking_details.setMoney_type(money_type);
            booking_details.setDay_booking(day_booking);
            booking_details.setUser_id_in(user_id_in);
            booking_details.setDay_out(day_out);
            booking_details.setUser_id_out(user_id_out);
            booking_details.setBooking_Room_ID(booking_room_id);

            this.booking_detailsService.update(booking_details);
            request.setAttribute("booking_details", booking_details);
            request.setAttribute("booking_rooms", booking_rooms);
            request.setAttribute("message", "Booking detail was updated");
            dispatcher = request.getRequestDispatcher("booking_details/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Booking_Details booking_details = this.booking_detailsService.findById(id);
        List<Booking_Rooms> booking_rooms = this.booking_roomsService.findAll();

        RequestDispatcher dispatcher;
        if (booking_details == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("booking_details", booking_details);
            request.setAttribute("booking_rooms", booking_rooms);
            dispatcher = request.getRequestDispatcher("booking_details/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteBooking_Details(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Booking_Details booking_details = this.booking_detailsService.findById(id);

        RequestDispatcher dispatcher;
        if (booking_details == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.booking_detailsService.remove(id, booking_details);
            try {
                response.sendRedirect("/booking_details");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Booking_Details booking_details = this.booking_detailsService.findById(id);

        RequestDispatcher dispatcher;
        if (booking_details == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("booking_details", booking_details);
            dispatcher = request.getRequestDispatcher("booking_details/delete.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
