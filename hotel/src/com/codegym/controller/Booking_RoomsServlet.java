package com.codegym.controller;

import com.codegym.model.Booking_Rooms;
import com.codegym.service.booking_rooms.Booking_RoomsJDBCServiceImpl;
import com.codegym.service.booking_rooms.Booking_RoomsService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Booking_RoomsServlet", urlPatterns = "/booking_rooms")
public class Booking_RoomsServlet extends HttpServlet {
    private Booking_RoomsService booking_roomsService = new Booking_RoomsJDBCServiceImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        listBooking_Rooms(request, response);
    }

    private void listBooking_Rooms(HttpServletRequest request, HttpServletResponse response) {
        List<Booking_Rooms> booking_rooms = this.booking_roomsService.findAll();
        request.setAttribute("booking_rooms", booking_rooms);

        RequestDispatcher dispatcher = request.getRequestDispatcher("booking_rooms/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
