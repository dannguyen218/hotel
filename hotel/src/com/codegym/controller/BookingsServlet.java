package com.codegym.controller;

import com.codegym.model.Booking_Deposits;
import com.codegym.model.Bookings;
import com.codegym.model.Customers;
import com.codegym.model.Users;
import com.codegym.service.bookings.BookingsJDBCServiceImpl;
import com.codegym.service.bookings.BookingsService;
import com.codegym.service.booking_deposits.Booking_DepositsJDBCServiceImpl;
import com.codegym.service.booking_deposits.Booking_DepositsService;
import com.codegym.service.customers.CustomersJDBCServiceImpl;
import com.codegym.service.customers.CustomersService;
import com.codegym.service.users.UsersJDBCServiceImpl;
import com.codegym.service.users.UsersService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "BookingsServlet", urlPatterns = "/bookings")
public class BookingsServlet extends HttpServlet {
    private BookingsService bookingsService = new BookingsJDBCServiceImpl();
    private UsersService usersService = new UsersJDBCServiceImpl();
    private Booking_DepositsService booking_depositsService = new Booking_DepositsJDBCServiceImpl();
    private CustomersService customersService = new CustomersJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                createBookings(request, response);
                break;
            case "edit":
                updateBookings(request, response);
                break;
            case "delete":
                deleteBookings(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewBookings(request, response);
                break;
            default:
                listBookings(request, response);
        }
    }

    private void listBookings(HttpServletRequest request, HttpServletResponse response) {
        List<Bookings> bookings = this.bookingsService.findAll();
        request.setAttribute("bookings", bookings);

        RequestDispatcher dispatcher = request.getRequestDispatcher("bookings/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void viewBookings(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Bookings bookings = this.bookingsService.findById(id);

        RequestDispatcher dispatcher;
        if (bookings == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("bookings", bookings);
            dispatcher = request.getRequestDispatcher("bookings/view.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createBookings(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        String day_reservation = request.getParameter("day_reservation");
        String day_check_in = request.getParameter("day_check_in");
        String day_check_out = request.getParameter("day_check_out");
        String payments = request.getParameter("payments");
        String amount_all = request.getParameter("amount_all");
        String money_type = request.getParameter("money_type");
        int user_check_in = Integer.parseInt(request.getParameter("user_check_in"));
        int user_check_out = Integer.parseInt(request.getParameter("user_check_out"));
        int users_id = Integer.parseInt(request.getParameter("listUsers"));
        int booking_deposits_id = Integer.parseInt(request.getParameter("listBooking_Deposits"));
        int customers_id = Integer.parseInt(request.getParameter("listCustomers"));

        Bookings bookings = new Bookings(day_reservation, day_check_in, day_check_out, payments, amount_all, money_type, user_check_in, user_check_out, users_id, booking_deposits_id, customers_id);
        this.bookingsService.save(bookings);

        List<Users> users = this.usersService.findAll();
        List<Booking_Deposits> booking_deposits = this.booking_depositsService.findAll();
        List<Customers> customers = this.customersService.findAll();
        request.setAttribute("users", users);
        request.setAttribute("booking_deposits", booking_deposits);
        request.setAttribute("customers", customers);
        dispatcher = request.getRequestDispatcher("bookings/create.jsp");
        request.setAttribute("message", "New booking was created");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        List<Users> users = this.usersService.findAll();
        List<Booking_Deposits> booking_deposits = this.booking_depositsService.findAll();
        List<Customers> customers = this.customersService.findAll();
        request.setAttribute("users", users);
        request.setAttribute("booking_deposits", booking_deposits);
        request.setAttribute("customers", customers);

        RequestDispatcher dispatcher = request.getRequestDispatcher("bookings/create.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateBookings(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        String day_reservation = request.getParameter("day_reservation");
        String day_check_in = request.getParameter("day_check_in");
        String day_check_out = request.getParameter("day_check_out");
        String payments = request.getParameter("payments");
        String amount_all = request.getParameter("amount_all");
        String money_type = request.getParameter("money_type");
        int user_check_in = Integer.parseInt(request.getParameter("user_check_in"));
        int user_check_out = Integer.parseInt(request.getParameter("user_check_out"));
        int users_id = Integer.parseInt(request.getParameter("listUsers"));
        int booking_deposits_id = Integer.parseInt(request.getParameter("listBooking_Deposits"));
        int customers_id = Integer.parseInt(request.getParameter("listCustomers"));
        Bookings bookings = this.bookingsService.findById(id);

        List<Users> users = this.usersService.findAll();
        List<Booking_Deposits> booking_deposits = this.booking_depositsService.findAll();
        List<Customers> customers = this.customersService.findAll();

        RequestDispatcher dispatcher;
        if (bookings == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            bookings.setDay_reservation(day_reservation);
            bookings.setDay_check_in(day_check_in);
            bookings.setDay_check_out(day_check_out);
            bookings.setPayments(payments);
            bookings.setAmount_all(amount_all);
            bookings.setMoney_type(money_type);
            bookings.setUser_check_in(user_check_in);
            bookings.setUser_check_out(user_check_out);
            bookings.setUsers_ID(users_id);
            bookings.setBooking_Deposits_ID(booking_deposits_id);
            bookings.setCustomers_ID(customers_id);

            this.bookingsService.update(bookings);
            request.setAttribute("bookings", bookings);
            request.setAttribute("users", users);
            request.setAttribute("booking_deposits", booking_deposits);
            request.setAttribute("customers", customers);
            request.setAttribute("message", "Booking was updated");
            dispatcher = request.getRequestDispatcher("bookings/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Bookings bookings = this.bookingsService.findById(id);
        List<Users> users = this.usersService.findAll();
        List<Booking_Deposits> booking_deposits = this.booking_depositsService.findAll();
        List<Customers> customers = this.customersService.findAll();

        RequestDispatcher dispatcher;
        if (bookings == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("bookings", bookings);
            request.setAttribute("users", users);
            request.setAttribute("booking_deposits", booking_deposits);
            request.setAttribute("customers", customers);
            dispatcher = request.getRequestDispatcher("bookings/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteBookings(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Bookings bookings = this.bookingsService.findById(id);

        RequestDispatcher dispatcher;
        if (bookings == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.bookingsService.remove(id, bookings);
            try {
                response.sendRedirect("/bookings");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id"));
        Bookings bookings = this.bookingsService.findById(id);

        RequestDispatcher dispatcher;
        if (bookings == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("bookings", bookings);
            dispatcher = request.getRequestDispatcher("bookings/delete.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
