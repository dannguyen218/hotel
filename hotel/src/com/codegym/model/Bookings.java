package com.codegym.model;

public class Bookings {
    private int id;
    private String day_reservation;
    private String day_check_in;
    private String day_check_out;
    private String payments;
    private String amount_all;
    private String money_type;
    private int user_check_in;
    private int user_check_out;
    private int users_ID;
    private int booking_Deposits_ID;
    private int customers_ID;
    private int isDelete;
    private String deleted_at;
    private String deleted_by = "admin";
    private String updated_at;
    private String updated_by = "admin";
    private String created_at;
    private String created_by = "admin";

    public Bookings(String day_reservation, String day_check_in, String day_check_out, String payments, String amount_all, String money_type, int user_check_in, int user_check_out, int users_id, int booking_deposits_id, int customers_id) {
        this.setDay_reservation(day_reservation);
        this.setDay_check_in(day_check_in);
        this.setDay_check_out(day_check_out);
        this.setPayments(payments);
        this.setAmount_all(amount_all);
        this.setMoney_type(money_type);
        this.setUser_check_in(user_check_in);
        this.setUser_check_out(user_check_out);
        setUsers_ID(users_id);
        setBooking_Deposits_ID(booking_deposits_id);
        setCustomers_ID(customers_id);
    }

    public Bookings(int id, String day_reservation, String day_check_in, String day_check_out, String payments, String amount_all, String money_type, int user_check_in, int user_check_out, int users_id, int booking_deposits_id, int customers_id) {
        this.setId(id);
        this.setDay_reservation(day_reservation);
        this.setDay_check_in(day_check_in);
        this.setDay_check_out(day_check_out);
        this.setPayments(payments);
        this.setAmount_all(amount_all);
        this.setMoney_type(money_type);
        this.setUser_check_in(user_check_in);
        this.setUser_check_out(user_check_out);
        setUsers_ID(users_id);
        setBooking_Deposits_ID(booking_deposits_id);
        setCustomers_ID(customers_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay_reservation() {
        return day_reservation;
    }

    public void setDay_reservation(String day_reservation) {
        this.day_reservation = day_reservation;
    }

    public String getDay_check_in() {
        return day_check_in;
    }

    public void setDay_check_in(String day_check_in) {
        this.day_check_in = day_check_in;
    }

    public String getDay_check_out() {
        return day_check_out;
    }

    public void setDay_check_out(String day_check_out) {
        this.day_check_out = day_check_out;
    }

    public String getPayments() {
        return payments;
    }

    public void setPayments(String payments) {
        this.payments = payments;
    }

    public String getAmount_all() {
        return amount_all;
    }

    public void setAmount_all(String amount_all) {
        this.amount_all = amount_all;
    }

    public String getMoney_type() {
        return money_type;
    }

    public void setMoney_type(String money_type) {
        this.money_type = money_type;
    }

    public int getUser_check_in() {
        return user_check_in;
    }

    public void setUser_check_in(int user_check_in) {
        this.user_check_in = user_check_in;
    }

    public int getUser_check_out() {
        return user_check_out;
    }

    public void setUser_check_out(int user_check_out) {
        this.user_check_out = user_check_out;
    }

    public int getUsers_ID() {
        return users_ID;
    }

    public void setUsers_ID(int users_ID) {
        this.users_ID = users_ID;
    }

    public int getBooking_Deposits_ID() {
        return booking_Deposits_ID;
    }

    public void setBooking_Deposits_ID(int booking_Deposits_ID) {
        this.booking_Deposits_ID = booking_Deposits_ID;
    }

    public int getCustomers_ID() {
        return customers_ID;
    }

    public void setCustomers_ID(int customers_ID) {
        this.customers_ID = customers_ID;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
