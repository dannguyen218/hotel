package com.codegym.model;

public class Customers {
    private int id;

    public Customers(int id) {
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
