package com.codegym.model;

import java.time.LocalDateTime;

public class Booking_Details {
    private int id;
    private String room_name;
    private String type_room;
    private String price;
    private String money_deposit;
    private String money_type;
    private String day_booking;
    private String user_id_in;
    private String day_out;
    private String user_id_out;
    private int booking_Room_ID;
    private int isDelete;
    private String deleted_at;
    private String deleted_by = "admin";
    private String updated_at;
    private String updated_by = "admin";
    private String created_at;
    private String created_by = "admin";

    public Booking_Details(String room_name, String type_room, String price, String money_deposit, String money_type, String day_booking, String user_id_in, String day_out, String user_id_out, int booking_Room_ID) {
        this.setRoom_name(room_name);
        this.setType_room(type_room);
        this.setPrice(price);
        this.setMoney_deposit(money_deposit);
        this.setMoney_type(money_type);
        this.setDay_booking(day_booking);
        this.setUser_id_in(user_id_in);
        this.setDay_out(day_out);
        this.setUser_id_out(user_id_out);
        this.setBooking_Room_ID(booking_Room_ID);
    }

    public Booking_Details(int id, String room_name, String type_room, String price, String money_deposit, String money_type, String day_booking, String user_id_in, String day_out, String user_id_out, int booking_Room_ID) {
        this.setId(id);
        this.setRoom_name(room_name);
        this.setType_room(type_room);
        this.setPrice(price);
        this.setMoney_deposit(money_deposit);
        this.setMoney_type(money_type);
        this.setDay_booking(day_booking);
        this.setUser_id_in(user_id_in);
        this.setDay_out(day_out);
        this.setUser_id_out(user_id_out);
        this.setBooking_Room_ID(booking_Room_ID);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getType_room() {
        return type_room;
    }

    public void setType_room(String type_room) {
        this.type_room = type_room;
    }

    public String getMoney_type() {
        return money_type;
    }

    public void setMoney_type(String money_type) {
        this.money_type = money_type;
    }

    public String getUser_id_in() {
        return user_id_in;
    }

    public void setUser_id_in(String user_id_in) {
        this.user_id_in = user_id_in;
    }

    public String getUser_id_out() {
        return user_id_out;
    }

    public void setUser_id_out(String user_id_out) {
        this.user_id_out = user_id_out;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMoney_deposit() {
        return money_deposit;
    }

    public void setMoney_deposit(String money_deposit) {
        this.money_deposit = money_deposit;
    }

    public String getDay_booking() {
        return day_booking;
    }

    public void setDay_booking(String day_booking) {
        this.day_booking = day_booking;
    }

    public String getDay_out() {
        return day_out;
    }

    public void setDay_out(String day_out) {
        this.day_out = day_out;
    }

    public int getBooking_Room_ID() {
        return booking_Room_ID;
    }

    public void setBooking_Room_ID(int booking_Room_ID) {
        this.booking_Room_ID = booking_Room_ID;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }


//    private String date() {
////        LocalDateTime localDateTime = LocalDateTime.now();
////        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
////        return localDateTime.format(dateTimeFormatter);
//
//        LocalDateTime now = LocalDateTime.now();
//        return String.valueOf(now).toString();
//    }

}
