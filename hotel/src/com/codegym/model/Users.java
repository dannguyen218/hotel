package com.codegym.model;

public class Users {
    private int id;

    public Users(int id) {
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
