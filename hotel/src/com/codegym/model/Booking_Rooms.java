package com.codegym.model;

public class Booking_Rooms {
    private int id;

    public Booking_Rooms(int id) {
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
