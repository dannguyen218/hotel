package com.codegym.model;

public class Booking_Deposits {
    private int id;

    public Booking_Deposits(int id) {
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
