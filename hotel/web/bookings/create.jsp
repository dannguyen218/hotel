<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Create new booking</title>
    <style>
        .message {
            color: green;
        }
    </style>
</head>
<body>
<h1>Create booking</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<p>
    <a href="/bookings">Back to Bookings</a>
</p>

<form method="post">
    <fieldset>
        <legend>Booking information</legend>
        <table>
            <tr>
                <td>Day reservation: </td>
                <td><input type="text" name="day_reservation"></td>
            </tr>
            <tr>
                <td>Day check in: </td>
                <td><input type="text" name="day_check_in"></td>
            </tr>
            <tr>
                <td>Day check out: </td>
                <td><input type="text" name="day_check_out"></td>
            </tr>
            <tr>
                <td>Payments: </td>
                <td><input type="text" name="payments"></td>
            </tr>
            <tr>
                <td>Amount all: </td>
                <td><input type="text" name="amount_all"></td>
            </tr>
            <tr>
                <td>Money type: </td>
                <td><input type="text" name="money_type"></td>
            </tr>
            <tr>
                <td>User check in: </td>
                <td><input type="text" name="user_check_in"></td>
            </tr>
            <tr>
                <td>User check out: </td>
                <td><input type="text" name="user_check_out"></td>
            </tr>
            <tr>
                <td>Users ID: </td>
                <td>
                    <select name="listUsers">
                        <c:forEach items='${requestScope["users"]}' var="user">
                            <option value="${user.getId()}">${user.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Booking Deposits ID: </td>
                <td>
                    <select name="listBooking_Deposits">
                        <c:forEach items='${requestScope["booking_deposits"]}' var="booking_deposit">
                            <option value="${booking_deposit.getId()}">${booking_deposit.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Customers ID: </td>
                <td>
                    <select name="listCustomers">
                        <c:forEach items='${requestScope["customers"]}' var="customer">
                            <option value="${customer.getId()}">${customer.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create Booking" onclick="return confirm('Are you sure ?')"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
