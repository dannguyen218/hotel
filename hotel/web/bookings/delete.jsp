<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 13/11/2019
  Time: 01:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Deleting booking</title>
</head>
<body>
<h1>Deleting booking</h1>

<form method="post">
    <fieldset>
        <legend>Booking detail</legend>
        <table>
            <tr>
                <td>Day reservation: </td>
                <td>${requestScope["bookings"].getDay_reservation()}</td>
            </tr>
            <tr>
                <td>Day check in: </td>
                <td>${requestScope["bookings"].getDay_check_in()}</td>
            </tr>
            <tr>
                <td>Day check out: </td>
                <td>${requestScope["bookings"].getDay_check_out()}</td>
            </tr>
            <tr>
                <td>Payments: </td>
                <td>${requestScope["bookings"].getPayments()}</td>
            </tr>
            <tr>
                <td>Amount all: </td>
                <td>${requestScope["bookings"].getAmount_all()}</td>
            </tr>
            <tr>
                <td>Money type: </td>
                <td>${requestScope["bookings"].getMoney_type()}</td>
            </tr>
            <tr>
                <td>User check in: </td>
                <td>${requestScope["bookings"].getUser_check_in()}</td>
            </tr>
            <tr>
                <td>User check out: </td>
                <td>${requestScope["bookings"].getUser_check_out()}</td>
            </tr>
            <tr>
                <td>Users ID: </td>
                <td>${requestScope["bookings"].getUsers_ID()}</td>
            </tr>
            <tr>
                <td>Booking Deposits ID: </td>
                <td>${requestScope["bookings"].getBooking_Deposits_ID()}</td>
            </tr>
            <tr>
                <td>Customers ID: </td>
                <td>${requestScope["bookings"].getCustomers_ID()}</td>
            </tr>
            <tr>
                <td><input type="submit" value="Delete booking"
                           onclick="return confirm('Are you sure ?')"></td>
                <td><a href="/bookings">Back to bookings</a></td>
            </tr>
        </table>
    </fieldset>

</form>
</body>
</html>
