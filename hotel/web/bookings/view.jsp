<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>View booking</title>
</head>
<body>
<h1>Booking</h1>

<p>
    <a href="/bookings">Back to Bookings</a>
</p>
<table>
    <tr>
        <td>Day reservation: </td>
        <td>${requestScope["bookings"].getDay_reservation()}</td>
    </tr>
    <tr>
        <td>Day check in: </td>
        <td>${requestScope["bookings"].getDay_check_in()}</td>
    </tr>
    <tr>
        <td>Day check out: </td>
        <td>${requestScope["bookings"].getDay_check_out()}</td>
    </tr>
    <tr>
        <td>Payments: </td>
        <td>${requestScope["bookings"].getPayments()}</td>
    </tr>
    <tr>
        <td>Amount all: </td>
        <td>${requestScope["bookings"].getAmount_all()}</td>
    </tr>
    <tr>
        <td>Money type: </td>
        <td>${requestScope["bookings"].getMoney_type()}</td>
    </tr>
    <tr>
        <td>User check in: </td>
        <td>${requestScope["bookings"].getUser_check_in()}</td>
    </tr>
    <tr>
        <td>User check out: </td>
        <td>${requestScope["bookings"].getUser_check_out()}</td>
    </tr>
    <tr>
        <td>Users ID: </td>
        <td>${requestScope["bookings"].getUsers_ID()}</td>
    </tr>
    <tr>
        <td>Booking Deposits ID: </td>
        <td>${requestScope["bookings"].getBooking_Deposits_ID()}</td>
    </tr>
    <tr>
        <td>Customers ID: </td>
        <td>${requestScope["bookings"].getCustomers_ID()}</td>
    </tr>
</table>
</body>
</html>
