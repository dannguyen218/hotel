<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Booking Details</title>
</head>
<body>
<h1>Booking Details</h1>
<p>
    <a href="/">Back to home</a>
</p>


<table border="1">
    <tr>
        <th>ID</th>
    </tr>
    <c:forEach items='${requestScope["booking_rooms"]}' var="booking_room">
        <tr>
            <td>${booking_room.getId()}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>

