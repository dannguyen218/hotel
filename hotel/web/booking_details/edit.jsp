<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 13/11/2019
  Time: 01:11
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <title>Edit booking detail</title>
</head>
<body>
<h1>Edit booking detail</h1>

<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<form method="post">
    <fieldset>
        <legend>Booking Detail</legend>
        <table>
            <tr>
                <td>Room Name:</td>
                <td><input type="text" name="room_name" value="${requestScope["booking_details"].getRoom_name()}"></td>
            </tr>
            <tr>
                <td>Type Room:</td>
                <td><input type="text" name="type_room" value="${requestScope["booking_details"].getType_room()}"></td>
            </tr>
            <tr>
                <td>Price:</td>
                <td><input type="text" name="price" value="${requestScope["booking_details"].getPrice()}"></td>
            </tr>
            <tr>
                <td>Money Deposit:</td>
                <td><input type="text" name="money_deposit" value="${requestScope["booking_details"].getMoney_deposit()}"></td>
            </tr>
            <tr>
                <td>Money Type:</td>
                <td><input type="text" name="money_type" value="${requestScope["booking_details"].getMoney_type()}"></td>
            </tr>
            <tr>
                <td>Day Booking:</td>
                <td><input type="text" name="day_booking" value="${requestScope["booking_details"].getDay_booking()}"></td>
            </tr>
            <tr>
                <td>User Id In:</td>
                <td><input type="text" name="user_id_in" value="${requestScope["booking_details"].getUser_id_in()}"></td>
            </tr>
            <tr>
                <td>Day Out:</td>
                <td><input type="text" name="day_out" value="${requestScope["booking_details"].getDay_out()}"></td>
            </tr>
            <tr>
                <td>User Id Out:</td>
                <td><input type="text" name="user_id_out" value="${requestScope["booking_details"].getUser_id_out()}"></td>
            </tr>
            <tr>
                <td>Booking Room ID:</td>
                <td>
                    <select name="listBooking_Rooms">
                        <c:forEach items='${requestScope["booking_rooms"]}' var="booking_room">
                            <option value="${booking_room.getId()}"
                                ${requestScope["booking_details"].getBooking_Room_ID() == booking_room.getId() ? "selected" : ""}>
                                    ${booking_room.getId()}
                            </option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Update booking detail" onclick="return confirm('Are you sure ?')"></td>
                <td><a href="/booking_details">Back to booking details</a></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
