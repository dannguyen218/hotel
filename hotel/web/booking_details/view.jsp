<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 20:44
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>View booking detail</title>
</head>
<body>
<h1>Booking detail</h1>

<p>
    <a href="/booking_details">Back to Booking details</a>
</p>
<table>
    <tr>
        <td>Room Name: </td>
        <td>${requestScope["booking_details"].getRoom_name()}</td>
    </tr>
    <tr>
        <td>Type Room: </td>
        <td>${requestScope["booking_details"].getType_room()}</td>
    </tr>
    <tr>
        <td>Price: </td>
        <td>${requestScope["booking_details"].getPrice()}</td>
    </tr>
    <tr>
        <td>Money Deposit: </td>
        <td>${requestScope["booking_details"].getMoney_deposit()}</td>
    </tr>
    <tr>
        <td>Money Type: </td>
        <td>${requestScope["booking_details"].getMoney_type()}</td>
    </tr>
    <tr>
        <td>Day Booking: </td>
        <td>${requestScope["booking_details"].getDay_booking()}</td>
    </tr>
    <tr>
        <td>User Id In: </td>
        <td>${requestScope["booking_details"].getUser_id_in()}</td>
    </tr>
    <tr>
        <td>Day Out: </td>
        <td>${requestScope["booking_details"].getDay_out()}</td>
    </tr>
    <tr>
        <td>User Id Out: </td>
        <td>${requestScope["booking_details"].getUser_id_out()}</td>
    </tr>
    <tr>
        <td>Booking Room ID: </td>
        <td>${requestScope["booking_details"].getBooking_Room_ID()}</td>
    </tr>
</table>
</body>
</html>
