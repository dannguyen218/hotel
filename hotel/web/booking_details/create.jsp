<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Create new booking detail</title>
    <style>
        .message {
            color: green;
        }
    </style>
</head>
<body>
<h1>Create booking detail</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<p>
    <a href="/booking_details">Back to Booking details</a>
</p>

<form method="post">
    <fieldset>
        <legend>Booking detail information</legend>
        <table>
            <tr>
                <td>Room Name:</td>
                <td><input type="text" name="room_name"></td>
            </tr>
            <tr>
                <td>Type Room:</td>
                <td><input type="text" name="type_room"></td>
            </tr>
            <tr>
                <td>Price:</td>
                <td><input type="text" name="price"></td>
            </tr>
            <tr>
                <td>Money Deposit:</td>
                <td><input type="text" name="money_deposit"></td>
            </tr>
            <tr>
                <td>Money Type:</td>
                <td><input type="text" name="money_type"></td>
            </tr>
            <tr>
                <td>Day Booking:</td>
                <td><input type="text" name="day_booking"></td>
            </tr>
            <tr>
                <td>User Id In:</td>
                <td><input type="text" name="user_id_in"></td>
            </tr>
            <tr>
                <td>Day Out:</td>
                <td><input type="text" name="day_out"></td>
            </tr>
            <tr>
                <td>User Id Out:</td>
                <td><input type="text" name="user_id_out"></td>
            </tr>
            <tr>
                <td>Booking Room ID:</td>
                <td>
                    <select name="listBooking_Rooms">
                        <c:forEach items='${requestScope["booking_rooms"]}' var="booking_room">
                            <option value="${booking_room.getId()}">${booking_room.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create Booking Detail" onclick="return confirm('Are you sure ?')"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
