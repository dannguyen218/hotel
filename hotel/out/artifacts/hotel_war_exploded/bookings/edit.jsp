<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 13/11/2019
  Time: 01:11
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <title>Edit booking</title>
</head>
<body>
<h1>Edit booking</h1>

<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<form method="post">
    <fieldset>
        <legend>Booking</legend>
        <table>
            <tr>
                <td>Day reservation: </td>
                <td><input type="text" name="day_reservation" value="${requestScope["bookings"].getDay_reservation()}"></td>
            </tr>
            <tr>
                <td>Day check in: </td>
                <td><input type="text" name="day_check_in" value="${requestScope["bookings"].getDay_check_in()}"></td>
            </tr>
            <tr>
                <td>Day check out: </td>
                <td><input type="text" name="day_check_out" value="${requestScope["bookings"].getDay_check_out()}"></td>
            </tr>
            <tr>
                <td>Payments: </td>
                <td><input type="text" name="payments" value="${requestScope["bookings"].getPayments()}"></td>
            </tr>
            <tr>
                <td>Amount all: </td>
                <td><input type="text" name="amount_all" value="${requestScope["bookings"].getAmount_all()}"></td>
            </tr>
            <tr>
                <td>Money type: </td>
                <td><input type="text" name="money_type" value="${requestScope["bookings"].getMoney_type()}"></td>
            </tr>
            <tr>
                <td>User check in: </td>
                <td><input type="text" name="user_check_in" value="${requestScope["bookings"].getUser_check_in()}"></td>
            </tr>
            <tr>
                <td>User check out: </td>
                <td><input type="text" name="user_check_out" value="${requestScope["bookings"].getUser_check_out()}"></td>
            </tr>
            <tr>
                <td>Users ID: </td>
                <td>
                    <select name="listUsers">
                        <c:forEach items='${requestScope["users"]}' var="user">
                            <option value="${user.getId()}"
                                ${requestScope["bookings"].getUsers_ID() == user.getId() ? "selected" : ""}>
                                    ${user.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Booking Deposits ID: </td>
                <td>
                    <select name="listBooking_Deposits">
                        <c:forEach items='${requestScope["booking_deposits"]}' var="booking_deposit">
                            <option value="${booking_deposit.getId()}"
                                ${requestScope["bookings"].getBooking_Deposits_ID() == booking_deposit.getId() ? "selected" : ""}>
                                ${booking_deposit.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Customers ID: </td>
                <td>
                    <select name="listCustomers">
                        <c:forEach items='${requestScope["customers"]}' var="customer">
                            <option value="${customer.getId()}"
                                ${requestScope["bookings"].getCustomers_ID() == customer.getId() ? "selected" : ""}>
                                ${customer.getId()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Update booking" onclick="return confirm('Are you sure ?')"></td>
                <td><a href="/bookings">Back to bookings</a></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
