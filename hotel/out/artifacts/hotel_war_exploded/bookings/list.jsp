<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 12/11/2019
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Bookings</title>
</head>
<body>
<h1>Bookings</h1>
<p>
    <a href="/">Back to home</a>
</p>

<p>
    <a href="/bookings?action=create">Create new booking</a>
</p>

<table border="1">
    <tr>
        <th>Day reservation</th>
        <th>Day check in</th>
        <th>Day check out</th>
        <th>Payments</th>
        <th>Amount all</th>
        <th>Money type</th>
        <th>User check in</th>
        <th>User check out</th>
        <th>Users ID</th>
        <th>Booking Deposits ID</th>
        <th>Customers ID</th>
    </tr>
    <c:forEach items='${requestScope["bookings"]}' var="booking">
        <tr>
            <td><a href="/bookings?action=view&id=${booking.getId()}">${booking.getDay_reservation()}</a>
            </td>
            <td>${booking.getDay_check_in()}</td>
            <td>${booking.getDay_check_out()}</td>
            <td>${booking.getPayments()}</td>
            <td>${booking.getAmount_all()}</td>
            <td>${booking.getMoney_type()}</td>
            <td>${booking.getUser_check_in()}</td>
            <td>${booking.getUser_check_out()}</td>
            <td>${booking.getUsers_ID()}</td>
            <td>${booking.getBooking_Deposits_ID()}</td>
            <td>${booking.getCustomers_ID()}</td>
            <td><a href="/bookings?action=edit&id=${booking.getId()}">edit</a></td>
            <td><a href="/bookings?action=delete&id=${booking.getId()}">delete</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>

