<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Employees List</title>
</head>
<body>
<h1>Employees List</h1>
<p>
    <a href="/">Back to home</a>
</p>

<p>
    <a href="/employees?action=create">Create new employee</a>
</p>

<table border="1">
    <tr>
        <td>Employee Number</td>
        <td>Employee Name</td>
        <td>Email</td>
        <td>Job Title</td>
        <td>Office Code</td>
    </tr>
    <c:forEach items='${requestScope["employees"]}' var="employee">
        <tr>
            <td>${employee.getEmployeeNumber()}</td>
            <td><a href="/employees?action=view&employeeNumber=${employee.getEmployeeNumber()}">${employee.getEmployeeName()}</a></td>
            <td>${employee.getEmail()}</td>
            <td>${employee.getJobTitle()}</td>
            <td>${employee.getOfficeCode()}</td>
            <td><a href="/employees?action=edit&employeeNumber=${employee.getEmployeeNumber()}">edit</a></td>
            <td><a href="/employees?action=delete&employeeNumber=${employee.getEmployeeNumber()}">delete</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
