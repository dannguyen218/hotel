<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:46
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Create new employee</title>
    <style>
        .message{
            color:green;
        }
    </style>
</head>
<body>
<h1>Create new employee</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/employees">Back to employee list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Employee information</legend>
        <table>
            <tr>
                <td>Name: </td>
                <td><input type="text" name="employeeName"></td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><input type="text" name="email"></td>
            </tr>
            <tr>
                <td>Job Title: </td>
                <td><input type="text" name="jobTitle"></td>
            </tr>
            <tr>
                <td>Office Code: </td>
                <td><input type="text" name="officeCode"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create employee"></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>
