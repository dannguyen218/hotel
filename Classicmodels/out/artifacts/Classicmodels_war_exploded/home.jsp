<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<h1>Home</h1>

<p>
    <a href="/classicmodels?tb=customers">Customers</a>
</p>

<p>
    <a href="/classicmodels?tb=employees">Employees</a>
</p>
</body>
</html>
