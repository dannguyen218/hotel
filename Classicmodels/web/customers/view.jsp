<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>View customer</title>
</head>
<body>
<h1>Customer details</h1>

<p>
    <a href="/customers">Back to customer list</a>
</p>
<table>
    <tr>
        <td>Name: </td>
        <td>${requestScope["customer"].getCustomerName()}</td>
    </tr>
    <tr>
        <td>Phone: </td>
        <td>${requestScope["customer"].getPhone()}</td>
    </tr>
    <tr>
        <td>Address: </td>
        <td>${requestScope["customer"].getAddressLine()}</td>
    </tr>
    <tr>
        <td>City: </td>
        <td>${requestScope["customer"].getCity()}</td>
    </tr>
    <tr>
        <td>State: </td>
        <td>${requestScope["customer"].getState()}</td>
    </tr>
    <tr>
        <td>Postal Code: </td>
        <td>${requestScope["customer"].getPostalCode()}</td>
    </tr>
    <tr>
        <td>Country: </td>
        <td>${requestScope["customer"].getCountry()}</td>
    </tr>
    <tr>
        <td>salesRepEmployeeNumber: </td>
        <td>${requestScope["customer"].getSalesRepEmployeeNumber()}</td>
    </tr>
</table>
</body>
</html>
