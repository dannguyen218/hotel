<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Customer List</title>
</head>
<body>
<h1>Customer List</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<p>
    <a href="/">Back to home</a>
</p>

<p>
    <a href="/customers?action=create">Create new customer</a>
</p>

<table border="1">
    <tr>
        <td>Customer Name</td>
        <td>Phone</td>
        <td>Address Line</td>
        <td>City</td>
        <td>State</td>
        <td>Postal Code</td>
        <td>Country</td>
        <td>SalesRepEmployeeNumber</td>
    </tr>
    <c:forEach items='${requestScope["customers"]}' var="customer">
        <tr>
            <td><a href="/customers?action=view&customerNumber=${customer.getCustomerNumber()}">${customer.getCustomerName()}</a></td>
            <td>${customer.getPhone()}</td>
            <td>${customer.getAddressLine()}</td>
            <td>${customer.getCity()}</td>
            <td>${customer.getState()}</td>
            <td>${customer.getPostalCode()}</td>
            <td>${customer.getCountry()}</td>
            <td>${customer.getSalesRepEmployeeNumber()}</td>
            <td><a href="/customers?action=edit&customerNumber=${customer.getCustomerNumber()}">edit</a></td>
            <td><a href="/customers?action=delete&customerNumber=${customer.getCustomerNumber()}">delete</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
