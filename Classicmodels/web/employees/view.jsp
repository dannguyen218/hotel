<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta charset="UTF-8">
    <title>View employee</title>
</head>
<body>
<h1>Employee details</h1>

<p>
    <a href="/employees">Back to employee list</a>
</p>
<table>
    <tr>
        <td>Name: </td>
        <td>${requestScope["employee"].getEmployeeName()}</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>${requestScope["employee"].getEmail()}</td>
    </tr>
    <tr>
        <td>Job Title: </td>
        <td>${requestScope["employee"].getJobTitle()}</td>
    </tr>
    <tr>
        <td>Office Code: </td>
        <td>${requestScope["employee"].getOfficeCode()}</td>
    </tr>
</table>
</body>
</html>
