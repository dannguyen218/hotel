<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit employee</title>
</head>
<body>
<h1>Edit employee</h1>

<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<form method="post">
    <fieldset>
        <legend>Employee information</legend>
        <table>
            <tr>
                <td>Employee Number: </td>
                <td><input type="text" name="employeeId" value="${requestScope["employee"].getEmployeeNumber()}"></td>
            </tr>
            <tr>
                <td>Name: </td>
                <td><input type="text" name="employeeName" value="${requestScope["employee"].getEmployeeName()}"></td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><input type="text" name="email" value="${requestScope["employee"].getEmail()}"></td>
            </tr>
            <tr>
                <td>Job Title: </td>
                <td><input type="text" name="jobTitle" value="${requestScope["employee"].getJobTitle()}"></td>
            </tr>
            <tr>
                <td>Office Code: </td>
                <td><input type="text" name="officeCode" value="${requestScope["employee"].getOfficeCode()}"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Update employee"></td>
                <td><a href="/employees">Back to employee list</a></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>

