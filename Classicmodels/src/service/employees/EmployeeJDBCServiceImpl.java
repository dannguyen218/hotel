package service.employees;

import model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeJDBCServiceImpl implements EmployeeService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/classicmodels";
    private String jdbcUsername = "root";
    private String jdbcPassword = "12345678";

    private static final String INSERT_EMPLOYEE_SQL = "INSERT INTO employees (employeeName, email, jobTitle, officeCode) VALUES (?, ?, ?, ?);";
    private static final String SELECT_EMPLOYEE_BY_ID = "SELECT employeeNumber, employeeName, email, jobTitle, officeCode FROM employees WHERE employeeNumber = ?;";
    private static final String SELECT_ALL_EMPLOYEE = "SELECT * FROM employees;";
    private static final String DELETE_EMPLOYEE_SQL = "DELETE FROM employees WHERE employeeNumber = ?;";
    private static final String UPDATE_EMPLOYEE_SQL = "UPDATE employees SET employeeNumber = ?, employeeName = ?, email = ?, jobTitle = ?, officeCode = ? WHERE employeeNumber = ?;";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Employee> findAll() {
        List<Employee> users = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_EMPLOYEE)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int employeeNumber = rs.getInt("employeeNumber");
                String employeeName = rs.getString("employeeName");
                String email = rs.getString("email");
                String jobTitle = rs.getString("jobTitle");
                String officeCode = rs.getString("officeCode");
                users.add(new Employee(employeeNumber, employeeName, email, jobTitle, officeCode));
            }

        } catch (SQLException e) {
            printSQLException(e);
        }
        return users;
    }

    @Override
    public void save(Employee employee) {
        System.out.println(INSERT_EMPLOYEE_SQL);

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_EMPLOYEE_SQL)) {
            preparedStatement.setString(1, employee.getEmployeeName());
            preparedStatement.setString(2, employee.getEmail());
            preparedStatement.setString(3, employee.getJobTitle());
            preparedStatement.setString(4, employee.getOfficeCode());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Employee findById(int employeeNumber) {
        Employee employee = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_EMPLOYEE_BY_ID)) {
            preparedStatement.setInt(1, employeeNumber);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String employeeName = rs.getString("employeeName");
                String email = rs.getString("email");
                String jobTitle = rs.getString("jobTitle");
                String officeCode = rs.getString("officeCode");
                employee = new Employee(employeeNumber, employeeName, email, jobTitle, officeCode);
            }

        } catch (SQLException e) {
            printSQLException(e);
        }
        return employee;

    }

    @Override
    public void update(Employee employee) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_EMPLOYEE_SQL)) {
            statement.setInt(1, employee.getEmployeeNumber());
            statement.setString(2, employee.getEmployeeName());
            statement.setString(3, employee.getEmail());
            statement.setString(4, employee.getJobTitle());
            statement.setString(5, employee.getOfficeCode());
            statement.setInt(6, employee.getPrevEmployeeNumber());

            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public void remove(int employeeNumber) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_EMPLOYEE_SQL)) {
            statement.setInt(1, employeeNumber);
            statement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }
}
