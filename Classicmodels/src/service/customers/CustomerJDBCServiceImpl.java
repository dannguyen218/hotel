package service.customers;

import model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerJDBCServiceImpl implements CustomerService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/classicmodels";
    private String jdbcUsername = "root";
    private String jdbcPassword = "12345678";

    private static final String INSERT_USER_SQL = "INSERT INTO customers" + "(customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber, created_at, created_by) VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_USER_BY_ID = "SELECT customerNumber,customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber FROM customers WHERE customerNumber = ?;";
    private static final String SELECT_ALL_USERS = "SELECT * FROM customers WHERE isDelete = 0;";
    private static final String DELETE_USER_SQL = "UPDATE customers SET isDelete = 1, deleted_at = ?, deleted_by = ? WHERE customerNumber = ?;";
    private static final String UPDATE_USER_SQL = "UPDATE customers SET customerName = ?, phone = ?, addressLine = ?, city = ?, state = ?, postalCode = ?, country = ?, salesRepEmployeeNumber = ?, updated_at = ?, updated_by = ? WHERE customerNumber = ?;";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> users = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int customerNumber = rs.getInt("customerNumber");
                String customerName = rs.getString("customerName");
                String phone = rs.getString("phone");
                String addressLine = rs.getString("addressLine");
                String city = rs.getString("city");
                String state = rs.getString("state");
                String postalCode = rs.getString("postalCode");
                String country = rs.getString("country");
                int salesRepEmployeeNumber = rs.getInt("salesRepEmployeeNumber");
                users.add(new Customer(customerNumber, customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return users;
    }

    @Override
    public void save(Customer customer) {
        System.out.println(INSERT_USER_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {
            preparedStatement.setString(1, customer.getCustomerName());
            preparedStatement.setString(2, customer.getPhone());
            preparedStatement.setString(3, customer.getAddressLine());
            preparedStatement.setString(4, customer.getCity());
            preparedStatement.setString(5, customer.getState());
            preparedStatement.setString(6, customer.getPostalCode());
            preparedStatement.setString(7, customer.getCountry());
            preparedStatement.setInt(8, customer.getSalesRepEmployeeNumber());
            customer.setCreated_at();
            preparedStatement.setString(9, customer.getCreated_at());
            preparedStatement.setString(10, customer.getCreated_by());

            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Customer findById(int customerNumber) {
        Customer customer = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {
            preparedStatement.setInt(1, customerNumber);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String customerName = rs.getString("customerName");
                String phone = rs.getString("phone");
                String addressLine = rs.getString("addressLine");
                String city = rs.getString("city");
                String state = rs.getString("state");
                String postalCode = rs.getString("postalCode");
                String country = rs.getString("country");
                int salesRepEmployeeNumber = rs.getInt("salesRepEmployeeNumber");
                customer = new Customer(customerNumber, customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return customer;

    }

    @Override
    public void update(Customer customer) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL)) {
            statement.setString(1, customer.getCustomerName());
            statement.setString(2, customer.getPhone());
            statement.setString(3, customer.getAddressLine());
            statement.setString(4, customer.getCity());
            statement.setString(5, customer.getState());
            statement.setString(6, customer.getPostalCode());
            statement.setString(7, customer.getCountry());
            statement.setInt(8, customer.getSalesRepEmployeeNumber());
            customer.setUpdated_at();
            statement.setString(9, customer.getUpdated_at());
            statement.setString(10, customer.getUpdated_by());
            statement.setInt(11, customer.getCustomerNumber());


            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public void remove(int customerNumber, Customer customer) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USER_SQL)) {
            customer.setDeleted_at();
            statement.setString(1, customer.getDeleted_at());
            statement.setString(2, customer.getDeleted_by());
            statement.setInt(3, customerNumber);
            statement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }
}
