package model;

public class Employee {
    private int employeeNumber;
    private String employeeName;
    private String email;
    private String jobTitle;
    private String officeCode;
    private int prevEmployeeNumber;

    public Employee(String employeeName, String email, String jobTitle, String officeCode) {
        this.setEmployeeName(employeeName);
        this.setEmail(email);
        this.setJobTitle(jobTitle);
        this.setOfficeCode(officeCode);
    }

    public Employee(int employeeNumber, String employeeName, String email, String jobTitle, String officeCode) {
        this.setEmployeeNumber(employeeNumber);
        this.setEmployeeName(employeeName);
        this.setEmail(email);
        this.setJobTitle(jobTitle);
        this.setOfficeCode(officeCode);
        this.setPrevEmployeeNumber(employeeNumber);
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public int getPrevEmployeeNumber() {
        return prevEmployeeNumber;
    }

    public void setPrevEmployeeNumber(int prevEmployeeNumber) {
        this.prevEmployeeNumber = prevEmployeeNumber;
    }
}
