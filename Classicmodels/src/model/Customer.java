package model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Customer {
    private int customerNumber;
    private String customerName;
    private String phone;
    private String addressLine;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private int salesRepEmployeeNumber;
    private int isDelete = 0;
    private String deleted_at;
    private String deleted_by = "admin";
    private String updated_at;
    private String updated_by = "admin";
    private String created_at;
    private String created_by = "admin";

    public Customer(String customerName, String phone, String addressLine, String city, String state, String postalCode,
                    String country, int salesRepEmployeeNumber) {
        this.setCustomerName(customerName);
        this.setPhone(phone);
        this.setAddressLine(addressLine);
        this.setCity(city);
        this.setState(state);
        this.setPostalCode(postalCode);
        this.setCountry(country);
        this.setSalesRepEmployeeNumber(salesRepEmployeeNumber);
    }

    public Customer(int customerNumber, String customerName, String phone, String addressLine, String city, String state, String postalCode,
                    String country, int salesRepEmployeeNumber) {
        this.setCustomerNumber(customerNumber);
        this.setCustomerName(customerName);
        this.setPhone(phone);
        this.setAddressLine(addressLine);
        this.setCity(city);
        this.setState(state);
        this.setPostalCode(postalCode);
        this.setCountry(country);
        this.setSalesRepEmployeeNumber(salesRepEmployeeNumber);
    }

    public int getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getSalesRepEmployeeNumber() {
        return salesRepEmployeeNumber;
    }

    public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
        this.salesRepEmployeeNumber = salesRepEmployeeNumber;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at() {
        this.deleted_at = date();
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at() {
        this.updated_at = date();
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at() {
        this.created_at = date();
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    private String date() {
//        LocalDateTime localDateTime = LocalDateTime.now();
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        return localDateTime.format(dateTimeFormatter);

        LocalDateTime now = LocalDateTime.now();
        return Timestamp.valueOf(now).toString();
    }
}
