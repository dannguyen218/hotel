//package controller;
//
//import model.Customer;
//import model.Employee;
//import service.customers.CustomerJDBCServiceImpl;
//import service.customers.CustomerService;
//import service.employees.EmployeeJDBCServiceImpl;
//import service.employees.EmployeeService;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//
//@WebServlet(name = "CustomerServlet", urlPatterns = "/classicmodels")
//public class aaa extends HttpServlet {
//
//    private CustomerService customerService = new CustomerJDBCServiceImpl();
//    private EmployeeService employeeService = new EmployeeJDBCServiceImpl();
//
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String action = request.getParameter("action");
//        String tb = request.getParameter("tb");
//        if (action == null) {
//            action = "";
//        }
//        if (tb == null) {
//            tb = "";
//        }
//
//        switch (tb) {
//            case "customers":
//                actionPost(request, response, action);
//                break;
//            case "employees":
//                actionPost(request, response, action);
//                break;
//        }
//
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String action = request.getParameter("action");
//        String tb = request.getParameter("tb");
//        if (action == null) {
//            action = "";
//        }
//        if (tb == null) {
//            tb = "";
//        }
//
//
//        switch (tb) {
//            case "customers":
//                if (action == "")
//                    list(request, response);
//                else
//                    actionGet(request, response, action);
//                break;
//            case "employees":
//                if (action == "")
//                    list(request, response);
//                else
//                    actionGet(request, response, action);
//            default:
//                home(request, response);
//                break;
//        }
//    }
//
//    private void actionPost(HttpServletRequest request, HttpServletResponse response, String action) {
//        switch (action) {
//            case "create":
//                create(request, response);
//                break;
//            case "edit":
//                edit(request, response);
//                break;
//            case "delete":
//                delete(request, response);
//        }
//    }
//
//    private void actionGet(HttpServletRequest request, HttpServletResponse response, String action) {
//        switch (action) {
//            case "create":
//                showCreate(request, response);
//                break;
//            case "edit":
//                showEdit(request, response);
//                break;
//            case "delete":
//                showDelete(request, response);
//                break;
//            case "view":
//                view(request, response);
//                break;
//        }
//    }
//
//    private void list(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                List<Customer> customers = this.customerService.findAll();
//                request.setAttribute("customers", customers);
//                dispatcher = request.getRequestDispatcher("customers/list.jsp");
//                break;
//            case "employees":
//                List<Employee> employees = this.employeeService.findAll();
//                request.setAttribute("employees", employees);
//                dispatcher = request.getRequestDispatcher("employees/list.jsp");
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void view(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
//                Customer customer = this.customerService.findById(customerNumber);
//                if (customer == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("customer", customer);
//                    dispatcher = request.getRequestDispatcher("customers/view.jsp");
//                }
//                break;
//            case "employees":
//                int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
//                Employee employee = this.employeeService.findById(employeeNumber);
//                if (employee == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("employee", employee);
//                    dispatcher = request.getRequestDispatcher("employees/view.jsp");
//                }
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void delete(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
//                Customer customer = this.customerService.findById(customerNumber);
//
//                if (customer == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    this.customerService.remove(customerNumber);
//                    try {
//                        response.sendRedirect("/classicmodels?tb=customers");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                break;
//            case "employees":
//                int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
//                Employee employee = this.employeeService.findById(employeeNumber);
//
//                if (employee == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    this.employeeService.remove(employeeNumber);
//                    try {
//                        response.sendRedirect("/classicmodels?tb=employees");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//    }
//
//    private void showDelete(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
//                Customer customer = this.customerService.findById(customerNumber);
//
//                if (customer == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("customer", customer);
//                    dispatcher = request.getRequestDispatcher("customers/delete.jsp");
//                }
//                break;
//            case "employees":
//                int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
//                Employee employee = this.employeeService.findById(employeeNumber);
//
//                if (employee == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("employee", employee);
//                    dispatcher = request.getRequestDispatcher("employees/delete.jsp");
//                }
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void edit(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
//                String customerName = request.getParameter("customerName");
//                String phone = request.getParameter("phone");
//                String addressLine = request.getParameter("addressLine");
//                String city = request.getParameter("city");
//                String state = request.getParameter("state");
//                String postalCode = request.getParameter("postalCode");
//                String country = request.getParameter("country");
//                int salesRepEmployeeNumber = Integer.parseInt(request.getParameter("salesRepEmployeeNumber"));
//                Customer customer = this.customerService.findById(customerNumber);
//
//                if (customer == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    customer.setCustomerName(customerName);
//                    customer.setPhone(phone);
//                    customer.setAddressLine(addressLine);
//                    customer.setCity(city);
//                    customer.setState(state);
//                    customer.setPostalCode(postalCode);
//                    customer.setCountry(country);
//                    customer.setSalesRepEmployeeNumber(salesRepEmployeeNumber);
//
//                    this.customerService.update(customer);
//                    request.setAttribute("customer", customer);
//                    request.setAttribute("message", "Customer information was updated");
//                    dispatcher = request.getRequestDispatcher("customers/edit.jsp");
//                }
//                break;
//            case "employees":
//                int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
//                int employeeId = Integer.parseInt(request.getParameter("employeeId"));
//                String employeeName = request.getParameter("employeeName");
//                String email = request.getParameter("email");
//                String jobTitle = request.getParameter("jobTitle");
//                String officeCode = request.getParameter("officeCode");
//                Employee employee = this.employeeService.findById(employeeNumber);
//
//                if (employee == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    employee.setEmployeeNumber(employeeId);
//                    employee.setEmployeeName(employeeName);
//                    employee.setEmail(email);
//                    employee.setJobTitle(jobTitle);
//                    employee.setOfficeCode(officeCode);
//
//                    this.employeeService.update(employee);
//                    request.setAttribute("employee", employee);
//                    request.setAttribute("message", "Employee information was updated");
//                    dispatcher = request.getRequestDispatcher("employees/edit.jsp");
//                }
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void showEdit(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
//                Customer customer = this.customerService.findById(customerNumber);
//                if (customer == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("customer", customer);
//                    dispatcher = request.getRequestDispatcher("customers/edit.jsp");
//                }
//                break;
//            case "employees":
//                int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
//                Employee employee = this.employeeService.findById(employeeNumber);
//                if (employee == null) {
//                    dispatcher = request.getRequestDispatcher("error-404.jsp");
//                } else {
//                    request.setAttribute("employee", employee);
//                    dispatcher = request.getRequestDispatcher("employees/edit.jsp");
//                }
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void create(HttpServletRequest request, HttpServletResponse response) {
//
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                String customerName = request.getParameter("customerName");
//                String phone = request.getParameter("phone");
//                String addressLine = request.getParameter("addressLine");
//                String city = request.getParameter("city");
//                String state = request.getParameter("state");
//                String postalCode = request.getParameter("postalCode");
//                String country = request.getParameter("country");
//                int salesRepEmployeeNumber = Integer.parseInt(request.getParameter("salesRepEmployeeNumber"));
//
//                Customer customer = new Customer(customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber);
//                this.customerService.save(customer);
//
//                dispatcher = request.getRequestDispatcher("customers/create.jsp");
//                request.setAttribute("message", "New customer was created");
//                break;
//            case "employees":
//                String employeeName = request.getParameter("employeeName");
//                String email = request.getParameter("email");
//                String jobTitle = request.getParameter("jobTitle");
//                String officeCode = request.getParameter("officeCode");
//
//                Employee employee = new Employee(employeeName, email, jobTitle, officeCode);
//                this.employeeService.save(employee);
//
//                dispatcher = request.getRequestDispatcher("employees/create.jsp");
//                request.setAttribute("message", "New employee was created");
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void showCreate(HttpServletRequest request, HttpServletResponse response) {
//        String tb = request.getParameter("tb");
//        RequestDispatcher dispatcher;
//
//        switch (tb) {
//            case "customers":
//                dispatcher = request.getRequestDispatcher("customers/create.jsp");
//                break;
//            case "employees":
//                dispatcher = request.getRequestDispatcher("employees/create.jsp");
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + tb);
//        }
//
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void home(HttpServletRequest request, HttpServletResponse response) {
//        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
//        try {
//            dispatcher.forward(request, response);
//        } catch (ServletException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
//
