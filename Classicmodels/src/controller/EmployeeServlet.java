package controller;

import model.Employee;
import service.employees.EmployeeJDBCServiceImpl;
import service.employees.EmployeeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "EmployeeServlet", urlPatterns = "/employees")
public class EmployeeServlet extends HttpServlet {

    private EmployeeService employeeService = new EmployeeJDBCServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                createEmployee(request, response);
                break;
            case "edit":
                updateEmployee(request, response);
                break;
            case "delete":
                deleteEmployee(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewEmployee(request, response);
                break;
            default:
                listEmployee(request, response);
        }
    }


    private void listEmployee(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        List<Employee> employees = this.employeeService.findAll();
        request.setAttribute("employees", employees);
        dispatcher = request.getRequestDispatcher("employees/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void viewEmployee(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
        Employee employee = this.employeeService.findById(employeeNumber);
        if (employee == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("employee", employee);
            dispatcher = request.getRequestDispatcher("employees/view.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteEmployee(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
        Employee employee = this.employeeService.findById(employeeNumber);

        if (employee == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.employeeService.remove(employeeNumber);
            try {
                response.sendRedirect("/employees");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
        Employee employee = this.employeeService.findById(employeeNumber);

        if (employee == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("employee", employee);
            dispatcher = request.getRequestDispatcher("employees/delete.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateEmployee(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
        int employeeId = Integer.parseInt(request.getParameter("employeeId"));
        String employeeName = request.getParameter("employeeName");
        String email = request.getParameter("email");
        String jobTitle = request.getParameter("jobTitle");
        String officeCode = request.getParameter("officeCode");
        Employee employee = this.employeeService.findById(employeeNumber);

        if (employee == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            employee.setEmployeeNumber(employeeId);
            employee.setEmployeeName(employeeName);
            employee.setEmail(email);
            employee.setJobTitle(jobTitle);
            employee.setOfficeCode(officeCode);

            this.employeeService.update(employee);
            request.setAttribute("employee", employee);
            request.setAttribute("message", "Employee information was updated");
            dispatcher = request.getRequestDispatcher("employees/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
        Employee employee = this.employeeService.findById(employeeNumber);
        if (employee == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("employee", employee);
            dispatcher = request.getRequestDispatcher("employees/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createEmployee(HttpServletRequest request, HttpServletResponse response) {

        String tb = request.getParameter("tb");
        RequestDispatcher dispatcher;

        String employeeName = request.getParameter("employeeName");
        String email = request.getParameter("email");
        String jobTitle = request.getParameter("jobTitle");
        String officeCode = request.getParameter("officeCode");

        Employee employee = new Employee(employeeName, email, jobTitle, officeCode);
        this.employeeService.save(employee);

        dispatcher = request.getRequestDispatcher("employees/create.jsp");
        request.setAttribute("message", "New employee was created");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("employees/create.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

