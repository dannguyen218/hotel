package controller;

import model.Customer;
import model.Employee;
import service.customers.CustomerJDBCServiceImpl;
import service.customers.CustomerService;
import service.employees.EmployeeJDBCServiceImpl;
import service.employees.EmployeeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CustomerServlet", urlPatterns = "/customers")
public class CustomerServlet extends HttpServlet {

    private CustomerService customerService = new CustomerJDBCServiceImpl();
    private EmployeeService employeeService = new EmployeeJDBCServiceImpl();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }
        switch (action) {
            case "create":
                createCustomer(request, response);
                break;
            case "edit":
                updateCustomer(request, response);
                break;
            case "delete":
                deleteCustomer(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "";
        }

        switch (action) {
            case "create":
                showCreateForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                showDeleteForm(request, response);
                break;
            case "view":
                viewCustomer(request, response);
                break;
            default:
                listCustomers(request, response);
        }
    }

    private void createCustomer(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        String customerName = request.getParameter("customerName");
        String phone = request.getParameter("phone");
        String addressLine = request.getParameter("addressLine");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String postalCode = request.getParameter("postalCode");
        String country = request.getParameter("country");
        int salesRepEmployeeNumber = Integer.parseInt(request.getParameter("listEmployeeNumber"));

        Customer customer = new Customer(customerName, phone, addressLine, city, state, postalCode, country, salesRepEmployeeNumber);
        this.customerService.save(customer);

        List<Employee> employees = this.employeeService.findAll();
        request.setAttribute("employees", employees);
        dispatcher = request.getRequestDispatcher("customers/create.jsp");
        request.setAttribute("message", "New customer was created");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showCreateForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;
        List<Employee> employees = this.employeeService.findAll();
        request.setAttribute("employees", employees);
        dispatcher = request.getRequestDispatcher("customers/create.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteCustomer(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
        Customer customer = this.customerService.findById(customerNumber);

        if (customer == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            this.customerService.remove(customerNumber, customer);
            try {
                response.sendRedirect("/customers");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDeleteForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
        Customer customer = this.customerService.findById(customerNumber);

        if (customer == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("customer", customer);
            dispatcher = request.getRequestDispatcher("customers/delete.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateCustomer(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
        String customerName = request.getParameter("customerName");
        String phone = request.getParameter("phone");
        String addressLine = request.getParameter("addressLine");
        String city = request.getParameter("city");
        String state = request.getParameter("state");
        String postalCode = request.getParameter("postalCode");
        String country = request.getParameter("country");
        int salesRepEmployeeNumber = Integer.parseInt(request.getParameter("listEmployeeNumber"));
        Customer customer = this.customerService.findById(customerNumber);

        List<Employee> employees = this.employeeService.findAll();

        if (customer == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            customer.setCustomerName(customerName);
            customer.setPhone(phone);
            customer.setAddressLine(addressLine);
            customer.setCity(city);
            customer.setState(state);
            customer.setPostalCode(postalCode);
            customer.setCountry(country);
            customer.setSalesRepEmployeeNumber(salesRepEmployeeNumber);

            this.customerService.update(customer);
            request.setAttribute("customer", customer);
            request.setAttribute("employees", employees);
            request.setAttribute("message", "Customer information was updated");
            dispatcher = request.getRequestDispatcher("customers/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;
        int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
        Customer customer = this.customerService.findById(customerNumber);
        List<Employee> employees = this.employeeService.findAll();

        if (customer == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("customer", customer);
            request.setAttribute("employees", employees);
            request.setAttribute("message", customer.getCreated_at());
            dispatcher = request.getRequestDispatcher("customers/edit.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void viewCustomer(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;

        int customerNumber = Integer.parseInt(request.getParameter("customerNumber"));
        Customer customer = this.customerService.findById(customerNumber);
        if (customer == null) {
            dispatcher = request.getRequestDispatcher("error-404.jsp");
        } else {
            request.setAttribute("customer", customer);
            dispatcher = request.getRequestDispatcher("customers/view.jsp");
        }

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listCustomers(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;
        List<Customer> customers = this.customerService.findAll();
        request.setAttribute("customers", customers);
        dispatcher = request.getRequestDispatcher("customers/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

